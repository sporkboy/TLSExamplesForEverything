# About

This project is an attempt to create a catalog of examples of how to serve HTTPS in every language we can think of.

The intent is to provide proper examples of how to add TLS to your appliation and provide a jumping off point
so that developers can deliver secure applications no matter how small or larger they are and allow them to 
focus on the tasks at hand instead of putting off security, struggling with it or implementing it poorly.

Examples should load their certificate and key from environment variables or pull a file name from environment
variables and load the cert and key from the file.

Ultimately all examples should look for these variables at startup and failing to find them should generate
self-signed certificates.

# Contributing

If you wish to contiribute an example in a new language create a directory of the language name. 

All examples should provide a RESTFUL API. 
Currently examples reply to GET requests for /messages or /messages/{id} and return a response in the format:
{"Message":"Hello","User":""} and
{"Message":"Hello","User":"<id>"}
