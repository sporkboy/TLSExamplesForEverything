package main

import (
	"github.com/gorilla/mux"
	"fmt"
	"time"
	"net/http"
	"encoding/json"
	"log"
	"crypto/tls"
	"github.com/golang/glog"
	"crypto/rsa"
	"encoding/base64"
	"crypto/rand"
	"os"
	"math/big"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
)

var listenPort = 8443

var (
	keyPEM []byte
	certPEM []byte
)


type Message struct {
	Message string
	User string
}


// Initialize the application
func init() {
	// Fetch the key and certificate from environment variables.
	keyPEMStr := os.Getenv("SAMPLE_APP_PRIVATE_KEY")
	certPEMStr := os.Getenv("SAMPLE_APP_CERTIFICATE")

	// If either of those are empty
	if ( keyPEMStr == "" || certPEMStr == "" ) {
		// Generate a default key and cert
		privKey, pubKey := genKeyPair(2048)

		keyPEM = pem.EncodeToMemory(&pem.Block{Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(privKey)})
		certPEM = genCert(privKey, pubKey)
	} else {
		pemStr, err := base64.StdEncoding.DecodeString(keyPEMStr)
		if err != nil {
			glog.Fatalln(err)
		}
		certStr, err := base64.StdEncoding.DecodeString(certPEMStr)
		if err != nil {
			glog.Fatalln(err)
		}

		keyPEM = []byte(pemStr)
		certPEM = []byte(certStr)
	}

}

// Say hello world
func sayHello(w http.ResponseWriter, r *http.Request) {
	aMessage := Message{
		Message: "Hello",
		User: "World",
	}

	json.NewEncoder(w).Encode(aMessage)
}

// Say hello to a specific user
func sayHelloUser(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id, _ := params["id"]

	aMessage := Message{
		Message: "Hello",
		User: id,
	}

	json.NewEncoder(w).Encode(aMessage)
}

// Generate a private/public key pair for the specified key size.
func genKeyPair(keySize int) (*rsa.PrivateKey, *rsa.PublicKey){
	reader := rand.Reader

	privateKey, err := rsa.GenerateKey(reader, keySize)
	if err != nil {
		glog.Fatalln(err)
	}

	publicKey := privateKey.PublicKey

	return privateKey, &publicKey
}

// Generates a certificate for the provided key pair. Is currently returned as a []byte containing the certificate in PEM format.
func genCert(privKey *rsa.PrivateKey, pubKey *rsa.PublicKey) []byte {
	notBefore := time.Now()

	notAfter := notBefore.Add(365 * 24 * time.Hour)

	serialNumberLimit := new(big.Int).Lsh(big.NewInt(1), 128)
	serialNumber, err := rand.Int(rand.Reader, serialNumberLimit)
	if err != nil {
		log.Fatalf("failed to generate serial number: %s", err)
	}

	template := x509.Certificate{
		SerialNumber: serialNumber,
		Subject: pkix.Name{
			Organization: []string{"Nordstrom"},
		},
		NotBefore: notBefore,
		NotAfter:  notAfter,

		KeyUsage:              x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
		BasicConstraintsValid: true,
	}

	template.DNSNames = append(template.DNSNames, "localhost")
	template.DNSNames = append(template.DNSNames, "localhost.localdomain")

	derBytes, err := x509.CreateCertificate(rand.Reader, &template, &template, pubKey, privKey)
	if err != nil {
		log.Fatalf("Failed to create certificate: %s", err)
	}

	pemBytes := pem.EncodeToMemory(&pem.Block{Type: "CERTIFICATE", Bytes: derBytes})

	return pemBytes
}

// main function to boot up everything
func main() {

	router := mux.NewRouter()

	// Configure TLS
	cer, err := tls.X509KeyPair(certPEM, keyPEM)
	if err != nil {
		glog.Fatalln(err)
	}
	config := &tls.Config{Certificates: []tls.Certificate{cer}}
	listener, err := tls.Listen("tcp", fmt.Sprintf(":%d", listenPort), config)
	if err != nil {
		glog.Fatalln(err)
	}

	// Add some handlers
	router.HandleFunc("/message", sayHello).Methods("GET")
	router.HandleFunc("/message/{id}", sayHelloUser).Methods("GET")

	log.Printf("Server starting with port %d at: %s\n", listenPort, time.Now())

	log.Fatal(http.Serve(listener, router))

}