# About
The bare bones of an example in Python. It needs to have a configurable port, pull some things from environment variables and generate it's own self-signed certs.
